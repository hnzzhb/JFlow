﻿package BP.GPM;

import BP.DA.*;
import BP.Web.*;
import BP.En.*;

/** 
 用户菜单
 
*/
public class UserMenuAttr
{
	/** 
	 菜单
	 
	*/
	public static final String FK_Menu = "FK_Menu";
	/** 
	 用户
	 
	*/
	public static final String FK_Emp = "FK_Emp";
	/** 
	 是否选中.
	 
	*/
	public static final String IsChecked = "IsChecked";
}